[![pipeline status](https://gitlab.com/saepasomba/ppw-story7-sae-pasomba/badges/master/pipeline.svg)](https://gitlab.com/saepasomba/ppw-story7-sae-pasomba/commits/master)
[![coverage report](https://gitlab.com/saepasomba/ppw-story7-sae-pasomba/badges/master/coverage.svg)](https://gitlab.com/saepasomba/ppw-story7-sae-pasomba/commits/master)


**Link Story7**   : [Story 7 Sae](https://sae-pepew.herokuapp.com/showforms)

**Link Story8**   : [Story 8 Sae](https://sae-pepew.herokuapp.com)

> Losing is part of life. This **isn't tough**, this is **life**.