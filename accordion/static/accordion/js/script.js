$(".up").click(function() {
    var currentElement = $(this).parent().parent().parent();
    var previousElement = $(".accor" + (parseInt(currentElement.css("order")) - 1));

    var currentPanel = $(".panel" + currentElement.css("order"));
    var previousPanel = $(".panel" + previousElement.css("order"));

    var currentTmp = currentElement.css("order") - 1;
    var previousTmp = (parseInt(previousElement.css("order"))) + 1;


    if (currentElement.css("order") > 1) {
        previousElement.css("order", previousTmp);
        currentElement.css("order", currentTmp);

        currentElement.attr("class", "accor"+ currentTmp + " " + String(currentElement.attr("class")).substring(7));
        previousElement.attr("class", "accor"+ previousTmp + " " + String(previousElement.attr("class")).substring(7));

        console.log(currentPanel.attr("class"));
        console.log(previousPanel.attr("class"));
        currentPanel.attr("class", "panel" + currentTmp + " " + String(currentPanel.attr("class")).substring(7));
        previousPanel.attr("class", "panel" + previousTmp + " " + String(previousPanel.attr("class")).substring(7));
    };

})

$(".down").click(function() {
    var currentElement = $(this).parent().parent().parent();
    var nextElement = $(".accor" + (parseInt(currentElement.css("order")) + 1));

    var currentPanel = $(".panel" + currentElement.css("order"));
    var nextPanel = $(".panel" + nextElement.css("order"));

    var currentTmp = (parseInt(currentElement.css("order"))) + 1;
    var nextTmp = (parseInt(nextElement.css("order"))) - 1;

    console.log(currentTmp);
    console.log(nextTmp);

    if (currentElement.css("order") < 4) {
        nextElement.css("order", nextTmp);
        currentElement.css("order", currentTmp);


        currentElement.attr("class", "accor"+currentTmp + " " + String(currentElement.attr("class")).substring(7));
        nextElement.attr("class", "accor"+ nextTmp + " " + String(nextElement.attr("class")).substring(7));

        currentPanel.attr("class", "panel" + currentTmp + " " + String(currentPanel.attr("class")).substring(7));
        nextPanel.attr("class", "panel" + nextTmp + " " + String(nextPanel.attr("class")).substring(7));
    };
    
});

$(".accordion").click(function() {
    console.log($(event.target).is("button"))
    if ($(event.target).is("button")) {
        event.preventDefault();
    } else {

        var panelTarget = $(".panel" + $(this).css("order"));

        if (panelTarget.attr("class").includes("active")) {
            var activePanel = $(".active");
            var activeAccor = activePanel.parent();

            activePanel.css("display", "none");
            activePanel.removeClass("active");
            activeAccor.removeClass("activeAccor")
        } else {
            var panel = $(".panel" + $(this).css("order"));
            var activePanel = $(".active");

            var activeAccor = activePanel.parent();
            var accor = panel.parent()

            activePanel.css("display", "none");
            activePanel.removeClass("active");
            activeAccor.removeClass("activeAccor");
            
            panel.addClass("active")
            accor.addClass("activeAccor")
            panel.css("display", "block");
        };
    };
});

$(".themeChanger").click(function() {
    if (String($(this).attr("class")).includes("1")) {
        $("body").css("background-image", "linear-gradient(to bottom right, #543864, #202040)");
        $(".themeChanger").removeClass("themeChanger1");
        $(".themeChanger").addClass("themeChanger2");
    } else {
        $("body").css("background-image", "linear-gradient(to bottom right, #ffecc7, #f5b971)")
        $(".themeChanger").removeClass("themeChanger2");
        $(".themeChanger").addClass("themeChanger1");
    }
})

$(".accordionChanger").click(function() {
    if (String($(this).attr("class")).includes("1")) {
        $(".accordion").css("border-radius", "0");
        $(".panel").css("border-radius", "0");
        $(".accordionChanger").removeClass("accordionChanger1");
        $(".accordionChanger").addClass("accordionChanger2");
    } else {
        $(".accordion").css("border-radius", "25px");
        $(".panel").css("border-radius", "25px");
        $(".accordionChanger").removeClass("accordionChanger2");
        $(".accordionChanger").addClass("accordionChanger1");
    }
})
