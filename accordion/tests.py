from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import showAccordion
from .apps import AccordionConfig

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class AppsTesting(TestCase):
    def test_apps(self):
        self.assertEqual(AccordionConfig.name, 'accordion')
        self.assertEqual(apps.get_app_config('accordion').name, 'accordion')

class RoutingTest1(TestCase):
    def test_Accordion_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_Accordion_page_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'accordion.html')
    
    def test_Accordion_page_func(self):
        func = resolve('/')
        self.assertEqual(func.func, showAccordion)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    
    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest, self).tearDown()
    
    def test_dropdown_accordion(self):
        self.driver.get(self.live_server_url)
        accordion = self.driver.find_element_by_class_name('accor1')
        panel = self.driver.find_element_by_class_name('panel1')

        self.assertEqual('none', panel.value_of_css_property('display'))

        accordion.click()

        self.assertEqual('block', panel.value_of_css_property('display'))
    
    def test_down(self):
        self.driver.get(self.live_server_url)
        downButton = self.driver.find_element_by_name('down2')

        accord2 = self.driver.find_element_by_class_name('accor2').text

        downButton.click()

        accord3 = self.driver.find_element_by_class_name('accor3').text
    
        self.assertEqual(accord2, accord3)

    def test_up(self):
        self.driver.get(self.live_server_url)
        upButton = self.driver.find_element_by_name('up2')

        accord2 = self.driver.find_element_by_class_name('accor2').text

        upButton.click()

        accord1 = self.driver.find_element_by_class_name('accor1').text

        self.assertEqual(accord1, accord2)

    def test_changeTheme(self):
        self.driver.get(self.live_server_url)

        changeThemeButton = self.driver.find_element_by_class_name('themeChanger')

        theme = self.driver.find_element_by_tag_name('body')
        
        themeBefore = theme.value_of_css_property('background-color')

        changeThemeButton.click()

        themeAfter = theme.value_of_css_property('background-color')

        self.assertEqual(themeBefore, themeAfter)

    def test_accordionChanger(self):
        self.driver.get(self.live_server_url)

        changeShapeButton = self.driver.find_element_by_class_name('accordionChanger')
        
        shape = self.driver.find_element_by_class_name('accordion')
        
        shapeBefore = shape.value_of_css_property('border-radius')

        changeShapeButton.click()

        shapeAfter = shape.value_of_css_property('border-radius')

        self.assertNotEqual(25, shapeAfter)