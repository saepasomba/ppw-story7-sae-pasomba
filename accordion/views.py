from django.shortcuts import render

# Create your views here.
def showAccordion(request):
    return render(request, 'accordion.html')