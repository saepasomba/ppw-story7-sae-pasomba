from django.db import models
from django.forms import ModelForm

# Create your models here.
class Person(models.Model):
    nama = models.CharField('Nama', max_length=50)
    pesan = models.TextField('Pesan')
    warnaR = models.CharField('Warna Red', max_length=50, default='0')
    warnaG = models.CharField('Warna Green', max_length=50, default='0')
    warnaB = models.CharField('Warna Blue', max_length=50, default='0')
    
    def __str__(self):
        return self.nama
