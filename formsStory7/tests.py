from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import showForms, confirmation
from .models import Person
from .forms import PersonForm
from .apps import Formsstory7Config

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class AppsTesting(TestCase):
    def test_apps(self):
        self.assertEqual(Formsstory7Config.name, 'formsStory7')
        self.assertEqual(apps.get_app_config('formsStory7').name, 'formsStory7')

class RoutingTest1(TestCase):
    def test_formsToFill_page(self):
        response = Client().get('/showforms/')
        self.assertEqual(response.status_code, 200)
    
    def test_formsToFill_page_html(self):
        response = Client().get('/showforms/')
        self.assertTemplateUsed(response, 'formsToFill.html')
    
    def test_formsToFill_page_func(self):
        func = resolve('/showforms/')
        self.assertEqual(func.func, showForms)

class RoutingTest2(TestCase):    
    def test_confirmation_page_func(self):
        func = resolve('/showforms/confirmation/')
        self.assertEqual(func.func, confirmation)
    
class ModelTest(TestCase):
    def test_object_created_from_form(self):
        dummyPerson = Person(nama = "dummyPerson", pesan = "loremipsum123123123")
        dummyPerson.save()
        self.assertEqual(Person.objects.all().count(), 1)
    
    def test_label_for_object(self):
        dummyObject = Person.objects.create(nama = "dummyPerson", pesan = "loremipsum123123123")
        self.assertEqual(dummyObject.__str__(), "dummyPerson")

class FormTest(TestCase):
    def test_form_valid(self):
        form = PersonForm({'nama': 'dummyPerson', 'pesan': 'loremipsum123123123123'})
        self.assertTrue(form.is_valid())
    
    def test_form_not_valid(self):
        form = PersonForm(data={})
        self.assertFalse(form.is_valid())

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    
    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest, self).tearDown()
    
    def test_form_confirmed(self):
        self.driver.get(self.live_server_url + "/showforms")
        namaFieldBox = self.driver.find_element_by_name('nama')
        namaFieldBox.send_keys('dummyName')

        pesanFieldBox = self.driver.find_element_by_name('pesan')
        pesanFieldBox.send_keys('loremipsum123123123123123')

        time.sleep(5)

        submitButton = self.driver.find_element_by_name('submitButton')
        submitButton.send_keys(Keys.RETURN)

        time.sleep(5)

        confirmButton = self.driver.find_element_by_name('confirm')
        confirmButton.send_keys(Keys.RETURN)

        time.sleep(5)

        self.assertIn('dummyName',self.driver.page_source)

    def test_form_cancelled(self):
        self.driver.get(self.live_server_url + "/showforms")
        namaFieldBox = self.driver.find_element_by_name('nama')
        namaFieldBox.send_keys('dummyName2')

        pesanFieldBox = self.driver.find_element_by_name('pesan')
        pesanFieldBox.send_keys('loremipsum123123123123123')

        time.sleep(5)

        submitButton = self.driver.find_element_by_name('submitButton')
        submitButton.send_keys(Keys.RETURN)

        time.sleep(5)

        confirmButton = self.driver.find_element_by_name('cancel')
        confirmButton.send_keys(Keys.RETURN)

        time.sleep(5)

        self.assertNotIn('dummyName2',self.driver.page_source)

    def test_change_color(self):
        self.driver.get(self.live_server_url + "/showforms")
        namaFieldBox = self.driver.find_element_by_name('nama')
        namaFieldBox.send_keys('dummyName')

        pesanFieldBox = self.driver.find_element_by_name('pesan')
        pesanFieldBox.send_keys('loremipsum123123123123123')

        time.sleep(5)

        submitButton = self.driver.find_element_by_name('submitButton')
        submitButton.send_keys(Keys.RETURN)

        time.sleep(5)

        confirmButton = self.driver.find_element_by_name('confirm')
        confirmButton.send_keys(Keys.RETURN)

        time.sleep(5)

        originalColor = self.driver.find_element_by_name('messageBox').value_of_css_property('background-color')

        changeColor = self.driver.find_element_by_name('changeColor')
        changeColor.click()

        time.sleep(5)

        afterColor = self.driver.find_element_by_name('messageBox').value_of_css_property('background-color')

        self.assertNotEqual(originalColor, afterColor)
