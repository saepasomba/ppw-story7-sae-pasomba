from django.urls import path
from . import views

app_name = 'formsStory7'

urlpatterns = [
    path('showforms/', views.showForms, name='showForms'),
    path('showforms/confirmation/', views.confirmation, name='confirmation'),
    path('/<int:pkTarget>/', views.changeColor, name='changeColor'),
]