from django.shortcuts import render, redirect
from .models import Person
from .forms import PersonForm
import random

# Create your views here.
def showForms(request):
    if request.method == 'POST':
        form = PersonForm(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            pesan = request.POST['pesan']
            warnaR = str(random.randint(0,255))
            warnaG = str(random.randint(0,255))
            warnaB = str(random.randint(0,255))
            Person.objects.create(nama = nama,pesan = pesan, warnaR = warnaR, warnaG = warnaR, warnaB = warnaB)

    dataPerson = Person.objects.all()
    form = PersonForm()

    return render(request, 'formsToFill.html', {'person':dataPerson, 'form':form})

def confirmation(request):
    if request.method == 'POST':
        form = PersonForm(request.POST)
        return render(request, 'confirmation.html', {'nama':form.data['nama'], 'pesan':form.data['pesan']})

def changeColor(request, pkTarget):
    target = Person.objects.get(pk = pkTarget)
    target.warnaR = str(random.randint(0,255))
    target.warnaG = str(random.randint(0,255))
    target.warnaB = str(random.randint(0,255))
    
    target.save()

    dataPerson = Person.objects.all()
    return redirect('formsStory7:showForms')